// Fill out your copyright notice in the Description page of Project Settings.
// Better to do without C++

#include "BT_Task_ClearValue.h"
#include "BehaviorTree/BlackboardComponent.h"

UBT_Task_ClearValue::UBT_Task_ClearValue()
{
	NodeName = TEXT("Clear Blackboard Value");
}

EBTNodeResult::Type UBT_Task_ClearValue::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());

	return EBTNodeResult::Succeeded;

}