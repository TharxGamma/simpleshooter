// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BT_Task_ClearValue.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API UBT_Task_ClearValue : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:

	UBT_Task_ClearValue();

protected:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory);
};
