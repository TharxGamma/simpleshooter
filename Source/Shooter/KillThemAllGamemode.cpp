// Fill out your copyright notice in the Description page of Project Settings.


#include "KillThemAllGamemode.h"
#include "GameFramework/Controller.h"
#include "EngineUtils.h"
#include "ShooterAIController.h"

void AKillThemAllGamemode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);
	UE_LOG(LogTemp, Warning, TEXT("Pawn Killed"));

	APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());
	if (PlayerController != nullptr)
	{
		EndGame(false);
	}

	// For loop over ShooterAI in world:
	for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
	{
		// Is not dead?
		if (!AIController->IsDead())
		{
			return;
		}
		EndGame(true);
		 
	}
	// End game
}

void AKillThemAllGamemode::EndGame(bool bIsPlayerWinner)
{
	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		bool bIsPlayerController = Controller->IsPlayerController();
		if (bIsPlayerWinner)
		{
			Controller->GameHasEnded(Controller->GetPawn(), bIsPlayerController);
		}
		else 
		{
			Controller->GameHasEnded(Controller->GetPawn(), !bIsPlayerController);
		}
	};
}
